module Task16
( flatten
) where

-- Problem 6
-- The goal of this problem is to write a Haskell function flatten that will flatten a twice-nested list. For
-- instance, we should get that flatten [[1,2,3], [3,2],[],[7,8,2]] evaluates to [1,2,3,3,2,7,8,2]
-- 1. First specify the type of flatten without using the Haskell system.
-- 2. Now write the function.

-- for every list, add elements to new list.

flatten :: [[t]] -> [t]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)


    