module Task14
    ( 
        cfrac
    ) where

-- A theorem in number theory states that every non-zero real number x can be written as a continued
-- fraction. This is a potentially infinite expression of the form
-- For rational numbers, the ai
-- ’s will eventually all be 0, so the continued fraction is finite; for irrational
-- numbers, the continued fraction will be infinite. See e.g. [1] for more.
-- The goal of this problem is to write a Haskell function cfrac that will, given a real number r and a
-- natural number n, finds the list of the first n numbers in the continued fraction expansion of r.
-- 1
-- 1. First specify the type of cfrac without using the Haskell system.
-- 2. Now write the function

-- append x = r->int
-- cfrac 1/(r-x) (n-1)

cfrac :: (RealFrac a, Integral b) => a -> b -> [b]
cfrac r 0 = []
cfrac r n = let x = round r
            in x : cfrac (1.0 / (r - fromIntegral x)) (n - 1)