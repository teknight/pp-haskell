module Task13
    ( ispalindrome
    ) where

import Task12

-- A palindrome is a string that is the same written forwards and backwards such as “Otto” or “Madam”.
-- The goal of this problem is to write a Haskell function ispalindrome that will determine if a string of
-- characters is a palindrome.
-- 1. First specify the type of ispalindrome without using the Haskell system.
-- 2. Now write the function


        
ispalindrome :: [Char] -> Bool
ispalindrome x = reverses x == x
