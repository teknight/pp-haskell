module Stutter
    ( stutter
    ) where

stutter :: (Eq a, Num a) => [t] -> a -> [t]
stutter [] n = []
stutter (x:xs) n = (repeat x n) ++ (stutter xs n)
                   where
                   repeat :: (Eq a, Num a) => t -> a -> [t] 
                   repeat e 0 = []
                   repeat e n = e : (repeat e (n-1))

                   -- OMG INDENTATION IS A THING, fuck this language - also Hans can't code for sjit
