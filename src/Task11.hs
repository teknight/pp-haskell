module Task11
    ( fib
    ) where

-- The goal of this problem is to write a Haskell function fib that finds the nth Fibonacci number.
-- Fib er bare n-1 + n-2 = n, nemt
-- 1. First specify the type of fib without using the Haskell system.
-- 2. Now write the function. Use it to find fib 3 and try to find fib 45. What is the problem here?
-- 3. What is the time complexity of fib?




fib :: (Eq a, Num a) => a -> a
fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

