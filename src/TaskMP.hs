module TaskMP
( validAL,
  lookupAL,
  findFunAL
) where

import qualified Data.Set as Set
    
validAL :: Ord t1 => [(t1, t2)] -> Bool
validAL x = length x == Set.size (Set.fromList (map fst x))

findFunAL :: Eq t1 => [(t1, t2)] -> (t1 -> t2)
findFunAL x = lambda x
    where
    lambda (x:xs) e | fst x == e = snd x  
                    | otherwise = lambda xs e


lookupAL :: Eq t1 => [(t1, t2)] -> t1 -> t2
lookupAL l x = findFunAL l x

-- A problem directly related to the miniproject
-- An association list is a representation of the graph of a finite function f as a list of pairs [x1, f(x1), . . . , xn, f(xn)].
-- For instance, the function defined by
-- f(1) = false
-- f(2) = true
-- f(3) = false
-- f(4) = true
-- can be represented by the association list [(1,false),(2,true),(3,false),(4,true)]. We say that an association
-- list is valid if it describes the graph of a function, that is, every argument is bound to precisely one value
-- in the list. So for a list to be valid, whenever (xi
-- , yi) and (xj , yj ) are both found in the list, then xi 6= xj .
-- The goal of this problem is to write three Haskell functions valid, findfun and lookup with the
-- following behaviour
-- • valid will tell us if a list of pairs is a valid association list.
-- • findfun will return the function associated with an association list.
-- • lookup will, given an association list l and an argument x find the function value of x if it exists
-- For each of these functions, you should specify its type before writing any code.
-- Please note: In the next session, we will use the Maybe type constructor to deal with returning a
-- non-value if a meaningful value does not exist. For now, it is perfectly fine to ignore this issue