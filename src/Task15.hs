module Task15
    ( mylast
    )
where


mylast :: [t] -> t
mylast [x] = x
mylast (x:xs) = mylast xs