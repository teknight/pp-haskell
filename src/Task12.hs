module Task12
    ( reverses
    ) where

-- The goal of this problem is to write a Haskell function reverse that will reverse a list such that e.g.
-- reverse [1,2,3] evaluates to [3,2,1].
-- 1. First specify the type of reverse without using the Haskell system.
-- 2. Now write the function.
-- Nemt, vi tager bare en liste og vender den omvendt xd xdxd

        
reverses :: [t] -> [t]
reverses [] = []
reverses (x:xs) = (reverses xs) ++ [x]
