module Task13Spec where

import Test.Hspec
import Task13

spec :: Spec
spec =
  describe "Task 1.3" $ do
    it "Checks if palindrome on string otto is true" $ ispalindrome "otto" `shouldBe` True
    it "Checks if palindrome on string ottos if false" $ ispalindrome "ottos" `shouldBe` False
    it "Checks if palindrome on string madam is true" $ ispalindrome "madam" `shouldBe` True


