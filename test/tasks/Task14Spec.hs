module Task14Spec where

import           Test.Hspec
import           Task14

spec :: Spec
spec = describe "task 1.4" $ do 
    it "cfrac works" $ 
        cfrac 3.245 4 `shouldBe` [3, 4, 12, 4]
    