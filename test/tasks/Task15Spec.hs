module Task15Spec where

import           Test.Hspec
import           Task15

spec :: Spec
spec = describe "Task 1.5" $ do
    it "Finds the last element in list [1, 2, 3]" $ mylast [1, 2, 3] `shouldBe` 3


