module Task16Spec where

import           Test.Hspec
import           Task16

spec :: Spec
spec = describe "task 1.6" $ do
    it "f[[1], [2], [3]] to [1, 2, 3]" $ 
        flatten [[1], [2], [3]] `shouldBe` [1, 2, 3]
    it "Flattens elements from [[1, 2], [], [2, 3], [3, 4]] to [1, 2, 2, 3, 3, 4]" $ 
        flatten [[1, 2], [], [2, 3], [3, 4]] `shouldBe` [1, 2, 2, 3, 3, 4]
    it "Flattens elements from lists" $
        flatten [[1], [2], [3]] `shouldBe` [1, 2, 3]
    it "Flattens elements from [[1, 2], [], [2, 3], [3, 4]] to [1, 2, 2, 3, 3, 4]" $ 
        flatten [[1, 2], [], [2, 3], [3, 4]] `shouldBe` [1, 2, 2, 3, 3, 4]


