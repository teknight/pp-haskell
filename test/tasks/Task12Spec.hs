module Task12Spec where

import Test.Hspec
import Task12

spec :: Spec
spec =
  describe "Task 1.2" $ do
    it "Reverses a list when given [1, 2, 3]" $ reverses [1, 2, 3] `shouldBe` [3, 2, 1]
    it "Reverses a list when given [1, 2, 3, 5, 7]" $ reverses [1, 2, 3, 5, 7] `shouldBe` [7, 5, 3, 2, 1]


