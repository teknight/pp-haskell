module TaskMPSpec where

import           Test.Hspec
import           TaskMP

spec :: Spec
spec = describe "task Mini project" $ do
    it "should return true for [(1, True), (2, False), (3, False)]" $ 
        validAL [(1, True), (2, False), (3, False)] `shouldBe` True
    it "should return false for [(1, True), (1, False), (3, False)]" $ 
        validAL [(1, True), (1, False), (3, False)] `shouldBe` False
    it "Should lookup in an AL" $
        (lookupAL [(1, True), (2, False), (3, False)] 1) `shouldBe` True
    it "Should lookup in an AL2" $
        (lookupAL [(1, True), (2, False), (3, False)] 2) `shouldBe` False
    it "Should fundfunAL in an AL2" $
        (findFunAL [(1, True), (2, False), (3, False)] 2) `shouldBe` False