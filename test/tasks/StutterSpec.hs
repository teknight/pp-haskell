module StutterSpec where

import Test.Hspec
import Stutter

spec :: Spec
spec =
  describe "Task Stutter" $ do
    it "Returns [1, 1, 1, 5, 5, 5, 6, 6, 6] when given [1, 5, 6] 3" $ 
      stutter[1, 5, 6] 3 `shouldBe` [1, 1, 1, 5, 5, 5, 6, 6, 6]
    it "Returns [1, 5, 6] when given [1, 5, 6] 1" $ 
      stutter[1, 5, 6] 1 `shouldBe` [1, 5, 6]

