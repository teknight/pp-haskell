module Task11Spec where

import Test.Hspec
import Task11

spec :: Spec
spec =
  describe "Task 1.1" $ do
    it "Returns the 1st fib number when given 1" $ fib 1 `shouldBe` 1
    it "Returns the 8th fib number when given 8" $ fib 8 `shouldBe` 21
    it "Returns 20th fib number when given 20" $ fib 20 `shouldBe`  6765
